package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(@NotNull String userId, @NotNull Project project);

    void add (@NotNull List<Project> projects);

    void add (@NotNull Project... projects);

    @NotNull
    Project add (@NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    void load (@NotNull List<Project> projects);

    void load (@NotNull Project... projects);

    void clear();
}
