package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectArgException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectCmdException;
import ru.rencredit.jschool.kuzyushin.tm.repository.CommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.ProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.TaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.UserRepository;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.service.*;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(
            taskService, projectService, userService, authService);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initUsers();
        initCommands(commandService.getCommandList());
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command: commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() {
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(@Nullable final String[] args){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAILED]");
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    @SneakyThrows
    private void parseArg(@NotNull final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new IncorrectArgException(arg);
        argument.execute();
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCmdException(cmd);
        authService.checkRoles(command.roles());
        command.execute();
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }
}
