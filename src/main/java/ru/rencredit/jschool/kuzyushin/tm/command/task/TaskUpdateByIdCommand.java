package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        if (serviceLocator != null) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            @Nullable final String id = TerminalUtil.nextLine();
            serviceLocator.getTaskService().findOneById(userId, id);
            System.out.println("ENTER NAME:");
            @Nullable final String name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            @Nullable final String description = TerminalUtil.nextLine();
            serviceLocator.getTaskService().updateOneById(userId, id, name, description);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
