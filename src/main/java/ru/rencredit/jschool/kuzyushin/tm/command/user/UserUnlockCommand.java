package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().unlockUserByLogin(login);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
