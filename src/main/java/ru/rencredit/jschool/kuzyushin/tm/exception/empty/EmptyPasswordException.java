package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }
}
