package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @NotNull
    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void add(final @NotNull String userId,
                    final @NotNull Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final @NotNull String userId,
                       final @NotNull Project project) {
        if (!userId.equals(project.getUserId())) return;
        this.projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(final @NotNull String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final @NotNull Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final @NotNull List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findOneById(final @NotNull String userId,
                               final @NotNull String id) {
        final @NotNull List<Project> projects = findAll(userId);
        for (final @NotNull Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByName(final @NotNull String userId,
                                 final @NotNull String name) {
        final @NotNull List<Project> projects = findAll(userId);
        for (final @NotNull Project project: projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(final @NotNull String userId,
                                  final @NotNull Integer index) {
        final @NotNull List<Project> projects = findAll(userId);
        if (!projects.isEmpty()) return projects.get(index);
        return null;
    }

    @Nullable
    @Override
    public Project removeByName(final @NotNull String userId,
                                final @NotNull String name) {
        final @Nullable Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final @Nullable Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeById(final @NotNull String userId,
                              final @NotNull String id) {
        final @Nullable Project project = findOneById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public void load(final @NotNull List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void load(final @NotNull Project... projects) {
        clear();
        add(projects);
    }

    @Override
    public void add(final @NotNull List<Project> projects) {
        for (final @NotNull Project project: projects) add(project);
    }

    @Override
    public void add(final @NotNull Project... projects) {
        for (final @NotNull Project project: projects) add(project);
    }

    @NotNull
    @Override
    public Project add(final @NotNull Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }
}
