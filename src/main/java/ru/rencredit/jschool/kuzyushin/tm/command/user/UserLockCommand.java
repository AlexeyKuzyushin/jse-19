package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().lockUserByLogin(login);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
