package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.file.Files;

public final class DataBase64LoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        @NotNull final String originalData = new String(Files.readAllBytes(file.toPath()));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(originalData);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (serviceLocator != null) {
            serviceLocator.getDomainService().load(domain);
        }
        else {
            System.out.println("[FAILED]");
            return;
        }
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
