package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateMiddleNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-middle-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().findById(id);
            System.out.println("ENTER MIDDLE NAME:");
            @Nullable final String middleName = TerminalUtil.nextLine();
            serviceLocator.getUserService().updateMiddleName(id, middleName);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
