package ru.rencredit.jschool.kuzyushin.tm.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BINARY FILE]");
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
