package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User add(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(final @NotNull String id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(final @NotNull String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeById(final @NotNull String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(final @NotNull String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @NotNull
    @Override
    public  User removeUser(final @NotNull User user) {
        users.remove(user);
        return user;
    }

    @Override
    public void load(final @NotNull List<User> users) {
        clear();
        add(users);
    }

    @Override
    public void load(final @NotNull User... users) {
        clear();
        add(users);
    }

    @Override
    public void add(final @NotNull List<User> users) {
        for (final @NotNull User user: users) add(user);
    }

    @Override
    public void add(final @NotNull User... users) {
        for (final @NotNull User user: users) add(user);
    }

    @Override
    public void clear() {
        users.clear();
    }
}
