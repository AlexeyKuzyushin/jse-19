package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ICommandService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }
}
