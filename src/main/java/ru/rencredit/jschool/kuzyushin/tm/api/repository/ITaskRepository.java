package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull String userId, @NotNull Task task);

    void add (@NotNull List<Task> tasks);

    void add (@NotNull Task... tasks);

    @NotNull
    Task add (@NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    void load (@NotNull List<Task> tasks);

    void load (@NotNull Task... tasks);

    void clear();
}
