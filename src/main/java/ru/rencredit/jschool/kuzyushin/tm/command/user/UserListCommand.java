package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST USERS]");

        if (serviceLocator != null) {
            @NotNull final List<User> users = serviceLocator.getUserService().findAll();
            int index = 1;
            for (@NotNull final User user: users) {
                System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
