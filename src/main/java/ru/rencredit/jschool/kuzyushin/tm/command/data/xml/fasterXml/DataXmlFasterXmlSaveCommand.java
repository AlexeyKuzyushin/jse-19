package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataXmlFasterXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-fasterxml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file";
    }


    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");

        @NotNull final Domain domain = new Domain();
        if (serviceLocator != null) {
            serviceLocator.getDomainService().export(domain);
        }
        else {
            System.out.println("[FAILED]");
            return;
        }

        @NotNull final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public @Nullable Role @Nullable [] roles() {
        return new Role[] { Role.ADMIN };
    }
}
