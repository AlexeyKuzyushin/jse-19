package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user login";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LOGIN]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().findById(id);
            System.out.println("ENTER LOGIN:");
            @Nullable final String login = TerminalUtil.nextLine();
            serviceLocator.getUserService().updateLogin(id, login);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
