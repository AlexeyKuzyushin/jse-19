package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        if (serviceLocator != null) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
            serviceLocator.getTaskService().removeOneByIndex(userId, index);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
