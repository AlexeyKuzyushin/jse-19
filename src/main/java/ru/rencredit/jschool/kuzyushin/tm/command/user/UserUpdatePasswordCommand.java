package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PASSWORD]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().findById(id);
            System.out.println("ENTER PASSWORD:");
            @Nullable final String password = TerminalUtil.nextLine();
            serviceLocator.getUserService().updatePassword(id, password);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
