package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateEmailCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-email";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user e-mail";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER E-MAIL]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().findById(id);
            System.out.println("ENTER EMAIL:");
            @Nullable final String email = TerminalUtil.nextLine();
            serviceLocator.getUserService().updateEmail(id, email);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
