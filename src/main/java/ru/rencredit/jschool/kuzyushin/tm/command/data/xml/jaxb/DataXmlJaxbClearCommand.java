package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataXmlJaxbClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove xml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE XML FILE]");
        @NotNull final File file = new File(DataConstant.FILE_XML_JAXB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
