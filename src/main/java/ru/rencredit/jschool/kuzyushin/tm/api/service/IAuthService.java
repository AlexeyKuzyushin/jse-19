package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

    void login(@Nullable String login, @Nullable String password);

    void logout();
}
