package ru.rencredit.jschool.kuzyushin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyUserIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectIndexException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyNameException;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(final @NotNull IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @SneakyThrows
    public void add(final @Nullable String userId,
                    final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable String userId,
                       final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    @SneakyThrows
    public @NotNull List<Project> findALl(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    @SneakyThrows
    public void create(final @Nullable String userId,
                       final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    @SneakyThrows
    public void create(final @Nullable String userId,
                       final @Nullable String name,
                       final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(final @Nullable String userId,
                               final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateOneById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findOneById(userId, id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateOneByIndex(
            final @Nullable String userId, final @Nullable Integer index,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void load(final @Nullable List<Project> projects) {
        if(projects == null) return;
        projectRepository.load(projects);
    }

    @Override
    public void load(final @Nullable Project... projects) {
        if(projects == null) return;
        projectRepository.load(projects);
    }

    @NotNull
    @Override
    public List<Project> getProjectList() {
        return projectRepository.findAll();
    }
}
