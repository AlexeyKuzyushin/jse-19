package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.FileInputStream;

public class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-fasterxml-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        if (serviceLocator != null) {
            serviceLocator.getDomainService().load(domain);
        }
        else {
            System.out.println("[FAILED]");
            return;
        }
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
